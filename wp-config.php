<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'Meltag@123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');

define( 'WP_MEMORY_LIMIT', '256M' );
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HgM`D5M7oBDjTVpWY$YHfNV9Kw<LnJTL_7+c,*,,:T|J55F(1i9.W{L G?oFb&cm');
define('SECURE_AUTH_KEY',  'vB9]1OrzR4#C9yZaD|&465.Tob@9nsGtA7HgktO=Jg|+L{1ZkL #>`]y<bG7(HfE');
define('LOGGED_IN_KEY',    'u2+_co!tQv$o[MX,ZD1`)J5C,-W 3w`sM2dcVB024fBI?4h%;TNYXDFa!Y(U].Dp');
define('NONCE_KEY',        '0{bT +e/+`+@j2B=B)KI>|<]a^!J]1EDd!v|pn!B/jAk<_l8*G+J-0vaXKk|d2?m');
define('AUTH_SALT',        'rP4xiX{Rv.j[Ba0uF?oODr br/8bt.JSqVn 2Ul%a@Tp}T+1K$k(|]|)OH-?$V2]');
define('SECURE_AUTH_SALT', 'Iu59>3DMGyRi- L8eE-M(ZYKzeFg@|_,X@Gc<U8lUL/XDM$5!a@{.%z}at21RQsP');
define('LOGGED_IN_SALT',   ':g$x+C!$c5+CC@=9Vi+K$a=-lCA5vRGh0RT827l7])Y>#;;xZC?qJ[R1T*z>cK<F');
define('NONCE_SALT',       'Pm;L#q&Iri0+B1$$KvLN[F16`O6n]mB.oH=a:qIQ!RIEhx Yj--U#>=d,zw[mXU;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
